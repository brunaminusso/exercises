<?php

function equalizeExpenses($expenses) 
{
    $totalExpenses = array_sum($expenses);
    $averageExpenses = $totalExpenses / count($expenses);
    $exchange = 0;

    foreach ($expenses as $expense) {
        $exchange += max(0, $averageExpenses - $expense);
    }

    return number_format($exchange, 2);
}

function readExpenses($numberStudents) 
{
    $expenses = [];

    for ($i = 0; $i < $numberStudents; $i++) {
        do {
            echo "Enter the expense amount for student " . ($i + 1) . ": ";
            $amountSpend = trim(fgets(STDIN));

            if (!is_numeric($amountSpend) || $amountSpend < 0 || $amountSpend > 10000) {
                echo "Please enter a valid numeric value between 0 and 10000.\n";
            }
        } while (!is_numeric($amountSpend) || $amountSpend < 0 || $amountSpend > 10000);

        $expenses[] = floatval(str_replace(',', '.', $amountSpend));
    }

    return $expenses;
}

while (true) {
    do {
        echo "Enter the number of students on the trip (or 0 to exit): ";
        $numberStudents = trim(fgets(STDIN));

        if (!is_numeric($numberStudents) || $numberStudents < 1 || $numberStudents > 1000) {
            echo "Please enter a valid numeric value between 1 and 1000.\n";
        }
    } while (!is_numeric($numberStudents) || $numberStudents < 1 || $numberStudents > 1000);

    $numberStudents = intval($numberStudents);

    if ($numberStudents == 0) {
        break;
    }

    $tripExpenses = readExpenses($numberStudents);
    $tripExchange = equalizeExpenses($tripExpenses);

    echo '$ ' . $tripExchange . PHP_EOL;
}
