<?php

while (true) {
    $numberStudents = intval(fgets(STDIN));

    if ($numberStudents < 1 || $numberStudents > 1000) {
        echo "The number of students must be between 1 and 1000." . PHP_EOL;
        continue;
    }

    if ($numberStudents == 0) {
        break;
    }

    $expenses = [];

    for ($i = 0; $i < $numberStudents; $i++) {
        $amountSpend = floatval(str_replace(',', '.', trim(fgets(STDIN))));

        if ($amountSpend <= 0 || $amountSpend > 10000) {
            echo "The amount spent by a student must be between 0.01 and 10000.00 dollars." . PHP_EOL;
            $i--;
            continue;
        }

        $expenses[] = $amountSpend;
    }

    $totalExpenses = array_sum($expenses);
    $averageExpenses = $totalExpenses / $numberStudents;
    $exchange = 0;

    foreach ($expenses as $expense) {
        $exchange += max(0, $averageExpenses - $expense);
    }

    echo '$ ' . number_format($exchange, 2) . PHP_EOL;
}
