## MOVA Parte 2: Exercícios

Este repositório contém dois exercícios de lógica escritos em PHP:

1. [Bloggo Shortcuts](https://www.beecrowd.com.br/judge/en/problems/view/1239)
    
Este script PHP implementa a tradução de um formato de entrada específico, substituindo atalhos por tags HTML. Ele suporta itálico usando underline ('_') e negrito usando asteriscos ('*'). Além disso, o script valida o comprimento da entrada e os caracteres permitidos, fornecendo mensagens de erro apropriadas em caso de erro.

2. [The Trip](https://www.beecrowd.com.br/judge/en/problems/view/1220)

Este script PHP gerencia despesas de viagem, permitindo aos usuários inserir informações sobre os gastos de cada estudante em uma viagem e calcula a quantia mínima de dinheiro que deve mudar de mãos para igualar todos os custos dos alunos. Para esse exercício foi desenvolvido uma solução de duas formas diferentes: um script mais simples usando um loop while e uma solução um pouco mais complexa usando funções.

## Pré-requisitos

**Git:**

Certifique-se de ter o Git instalado em seu sistema. Você pode baixá-lo [aqui](git-scm.com).

**PHP:**

Tenha o PHP instalado em seu sistema. Você pode baixar [aqui](php.net).

## Instalação

Clone este repositório no seu ambiente local usando o GitLab:

```bash
  git clone https://gitlab.com/seu-usuario/nome-do-repo.git
  cd my-project
```

## Funcionalidades

**Exercício Bloggo Shortcuts**

```bash
  cd BloggoShortcuts
  php bloggoShortcut.php
```
- Validação de Comprimento de Entrada: O script verifica se o comprimento do texto de entrada está entre 1 e 50 caracteres.
- Validação de Caracteres Permitidos: Garante que o texto contenha apenas caracteres alfabéticos, sublinhados ('_'), asteriscos ('*'), espaços e símbolos de pontuação (',', ';', '.', '!', '?', '-', '(', ')').
- Tradução de Atalhos para HTML:Substitui pares de underscores por tags <i> e </i> para representar texto em itálico. Substitui pares de asteriscos por tags <b> e </b> para representar texto em negrito.
- Mensagens de Erro: Em caso de erro nas validações, exibe mensagens indicando os problemas encontrados.

**Exercício The Trip**

Script mais simples
```bash
  cd TheTrip
  php .php
```
Script mais complexo
```bash
  cd TheTrip
  php .php
```
- Leitura do Número de estudantes: Os scripts solicitam ao usuário que insira o número de estudantes na viagem. É validado se o número está dentro do intervalo permitido (entre 1 e 1000) e se é um valor numérico.
- Leitura de Despesas: Dentro do loop, o script lê as despesas de cada estudante. As despesas são validadas para garantir que estejam entre 0,01 e 10000,00 dólares.
- Cálculo da Média de Despesas: O script calcula a média das despesas totais dos estudantes.
- Cálculo do Valor a ser Trocado: Para cada estudante, o script calcula a diferença entre a média de despesas e as despesas reais e determina quanto dinheiro cada estudante deve receber ou pagar para igualar as despesas entre eles.