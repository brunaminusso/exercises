<?php

while ($line = fgets(STDIN)) {
    $line = trim($line);

    if (strlen($line) < 1 || strlen($line) > 50) {
        echo "Error: Input length must be between 1 and 50 characters.\n";
        continue;
    }

    if (!preg_match('/^[a-zA-Z_ *;,.!?\-()]+$/', $line)) {
        echo "Error: Invalid characters in the input.\n";
        continue;
    }

    $line = str_replace('_', '<i>', $line);
    $line = str_replace('_', '</i>', $line);

    $line = str_replace('*', '<b>', $line);
    $line = str_replace('*', '</b>', $line);

    echo $line . PHP_EOL;
}
